const https = require('https');
const axios = require('axios');
let data = JSON.stringify({
  "component": "gitnew",
  "properties": {
  "versionName": "1.0.0.0"
  }
});

let config = {
  method: 'put',
  url: 'https://00ec-2405-201-8009-182c-901b-f684-a1eb-5312.ngrok.io/cli/component/integrate',
  headers: { 
    'Authorization': 'Basic YWRtaW46YWRtaW4=', 
    'Content-Type': 'application/json'
  },
  data : data,
  httpsAgent: new https.Agent({ rejectUnauthorized: false})
};

axios(config)
.then((response) => {
  console.log(JSON.stringify(response.data));
})
.catch((error) => {
  console.log(error);
});